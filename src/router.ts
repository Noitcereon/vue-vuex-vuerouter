import { createRouter, createWebHistory } from "vue-router";
import Home from "./components/Home.vue";
import GamesList from "./components/GamesList.vue";
import VuexGamesList from "./components/VuexGamesList.vue";

const routes = [
  { path: "/", component: Home },
  { path: "/games", component: GamesList },
  { path: "/vuex-games", component: VuexGamesList },
];

const router = createRouter({
  history: createWebHistory(),
  routes,
});

export default router;
