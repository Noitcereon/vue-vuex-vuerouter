export interface IGame {
  game_id: Number;
  name: String;
  game_description: String;
  game_state: String;
  nw_lat: Number;
  nw_lng: Number;
  se_lat: Number;
  se_lng: Number;
  rules: null | String;
}
