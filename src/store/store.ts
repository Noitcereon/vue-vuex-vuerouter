import { createStore } from "vuex";
import counterModule from "./modules/counterModule";
import gameModule from "./modules/gameModule";

const store = createStore({
  modules: {
    counterModule,
    gameModule
  },
});

export default store;
