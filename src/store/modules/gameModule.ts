import { Module } from "vuex";
import { IGame } from "../../interfaces/IGame";
import { ICommonResponse } from "../../interfaces/ICommonResponse";

const counterModule: Module<any, any> = {
  namespaced: true,
  state() {
    const games: IGame[] = [];
    return {
      games,
    };
  },
  mutations: {
    setGames(state, games: IGame[]) {
      state.games = games;
    },
  },
  actions: {
    async fetchGames(context) {
      try {
        const apiUrl = "https://dk-hvz-backend.herokuapp.com/api/game";

        const response = await fetch(apiUrl);

        if (response.ok) {
          const data: ICommonResponse<IGame[]> = await response.json();
          context.commit("setGames", data.payload);
        }
      } catch (error) {
        console.log(error);
      }
    },
  },
};

export default counterModule;
