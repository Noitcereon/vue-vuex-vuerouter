import { Module } from "vuex";

const counterModule: Module<any, any> = {
  namespaced: true,
  state() {
    return {
      count: 0,
    };
  },
  mutations: {
    incrementCounter(state: any) {
      state.count++;
    },
  },
  actions: {
    incrementCounterAction(context: any) {
      context.commit("incrementCounter");
    },
  },
};

export default counterModule;
