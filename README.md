# Vue 3 with VueRouter4 and Vuex4 (using TypeScript & Vite)
A project that demonstrates the use of Vue 3 with the dependencies "Vue Router 4" and "Vuex 4". 

## Recommended IDE Setup

- [VSCode](https://code.visualstudio.com/) + [Volar](https://marketplace.visualstudio.com/items?itemName=johnsoncodehk.volar)

## Installation

1. Clone or download this repository
2. Navigate to the root folder of the project in your terminal
3. Run the command `npm install`
4. You should now be able to run the app via `npm run dev` at localhost:3000

## Usage
There is not much to it. It is meant to be looked through. A few important points though:

- router.ts and store.ts are the imported into the app in main.ts and applied. This is required to make VueRouter4 and Vuex4 work.

- The store uses [__namespaced__ Vuex modules](https://vuex.vuejs.org/guide/modules.html#namespacing). Look at the docs for it if you don't know it.

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.


## License
[MIT](https://choosealicense.com/licenses/mit/)


## Type Support For `.vue` Imports in TS

Since TypeScript cannot handle type information for `.vue` imports, they are shimmed to be a generic Vue component type by default. In most cases this is fine if you don't really care about component prop types outside of templates. However, if you wish to get actual prop types in `.vue` imports (for example to get props validation when using manual `h(...)` calls), you can enable Volar's Take Over mode by following these steps:

1. Run `Extensions: Show Built-in Extensions` from VSCode's command palette, look for `TypeScript and JavaScript Language Features`, then right click and select `Disable (Workspace)`. By default, Take Over mode will enable itself if the default TypeScript extension is disabled.
2. Reload the VSCode window by running `Developer: Reload Window` from the command palette.

You can learn more about Take Over mode [here](https://github.com/johnsoncodehk/volar/discussions/471).
